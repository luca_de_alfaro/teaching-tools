# Copyright Luca de Alfaro, 2019
# BSD License

import argparse
import csv
import datetime
import json
import os
import shutil
import traceback
import yaml

from nbgrader.api import Gradebook, MissingEntry
from notebook_utils import Notebook, CannotImportMetadata

class NotebookFixer(object):
    
    AUTOGRADE_LINE = 'nbgrader autograde --course-dir={course_dir} --student="{email}" --force {hw}\n'
    
    def __init__(self, args):
        self.args = args
        with open(args.settings, 'r') as f:
            self.settings = yaml.load(f, Loader=yaml.SafeLoader)
        self.graded_dir = os.path.join(self.settings['class_path'], 'autograded')
        self.submission_dir = os.path.join(self.settings['class_path'], 'submitted')
        self.release_dir = os.path.join(self.settings['class_path'], 'release', args.assignment)

        # Reads the assigned notebook.
        assignment_file = None
        with os.scandir(self.release_dir) as it:
            for entry in it:
                if entry.name.endswith('.ipynb') and entry.is_file():
                    assignment_file = entry
                    break
        assert assignment_file is not None, "Assignment not found"
        with open(entry) as f:
            self.assigned_notebook = Notebook(json.load(f), args)
        
    def fix_all(self):
        """Fixes all broken notebooks."""
        # Create the connection to the database
        with Gradebook('sqlite:///'  + self.settings['class_path'] + '/gradebook.db') as gb:
            # Finds the assignment
            broken_emails = []
            for student in gb.students:
                try:
                    subm = gb.find_submission(args.assignment, student.id)
                    if subm.needs_manual_grade:
                        print("Fixing submission for:", student.id)
                        try:
                            self.fix_notebook(student.id)
                        except:
                            broken_emails.append(student.id)
                            traceback.print_exc()                    
                except:
                    pass
            return broken_emails

    def fix_notebook(self, email):
        """Fixes the notebook for a given student."""
        # Reads the student notebook.
        submitted_notebook = None
        student_submission_dir = os.path.join(self.submission_dir, email, self.args.assignment)
        student_graded_dir = os.path.join(self.graded_dir, email, self.args.assignment)
        with os.scandir(student_submission_dir) as it:
            for entry in it:
                if entry.is_file() and entry.name.endswith('.ipynb'):
                    with open(entry) as f:
                        submitted_notebook = Notebook(json.load(f), args)
                    break
        if submitted_notebook is not None:
            # This is the line that actually fixes the notebook. 
            # We replace in the notebook the cell metadata with the metadata in the
            # assigned notebook.
            submitted_notebook.import_metadata(self.assigned_notebook)
            # Renames original file.
            shutil.move(entry.path, entry.path + ".orig")
            # Writes notebook in place.
            with open(entry.path, mode='w', newline='') as out_f:
                json.dump(submitted_notebook.get_result(), out_f, indent=2)
            # Removes autograding result.
            with os.scandir(student_graded_dir) as gr:
                for graded_entry in gr:
                    if graded_entry.is_file() and graded_entry.name.endswith('.ipynb'):
                        os.remove(graded_entry.path)
            # Calls the autograder for this student.
            os.system(self.AUTOGRADE_LINE.format(
                course_dir=self.settings['class_path'], email=email, hw=args.assignment))
            print("Fixed", email)
        

def main(args):
    # Creates a notebook fixer. 
    notebook_fixer = NotebookFixer(args)   
    # ... and fixed everything.
    broken = notebook_fixer.fix_all()
    print("Fixing failed for:")
    for email in broken:
        print(email)
            
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--assignment', type=str, default=None,
                        help='Assignment to fix.')
    # Default is fine.
    parser.add_argument('--settings', type=str, default='settings.yaml',
                        help='Settings file to be used')
    args = parser.parse_args()
    main(args)