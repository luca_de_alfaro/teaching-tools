# Copyright Luca de Alfaro, 2019
# BSD License

import argparse
import csv
import datetime
import io
import os
import pickle
import random

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from apiclient.http import MediaIoBaseDownload

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly',
            'https://www.googleapis.com/auth/drive.readonly']
# https://www.googleapis.com/auth/spreadsheets for r/w

# Sample command line:
# $ python test_download.py --drc_spreadsheet_id=<id>
#      --spreadsheet_id=<id> --duration=3 --drc_column=G --deadline="2019-07-13 17:33"  -c ~/temp/downloads


def main(args):
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            try:
                creds.refresh(Request())
            except:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server()
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    sheet_service = build('sheets', 'v4', credentials=creds)
    drive_service = build('drive', 'v3', credentials=creds)

    # Call the Sheets API for the submissions.
    range_name = args.sheet + '!' + 'A2:Z'
    sheet = sheet_service.spreadsheets()
    result = sheet.values().get(spreadsheetId=args.spreadsheet_id,
                                range=range_name).execute()
    values = result.get('values', [])

    if not values:
        print('No data found.')
        return

    # Call the Sheet API for the DRC spreadsheet.
    if args.drc_spreadsheet_id is not None:
        drc_range_name = args.drc_sheet + '!' + 'A2:Z'
        drc_sheet = sheet_service.spreadsheets()
        drc_result = drc_sheet.values().get(spreadsheetId=args.drc_spreadsheet_id,
                                            range=drc_range_name).execute()
        drc_values = drc_result.get('values', [])

    # Figures out for each student their submission deadline.
    normal_deadline = None
    if args.deadline is not None:
        normal_deadline = datetime.datetime.strptime(args.deadline, "%Y-%m-%d %H:%M")

    # Applies the dilation for DRC students.
    dilation_factors = {}
    drc_email_idx = ord(args.drc_email_column) - ord("A")
    drc_dilation_idx = ord(args.drc_column) - ord("A")
    for row in drc_values:
        email = row[drc_email_idx]
        dilation = row[drc_dilation_idx] if drc_dilation_idx < len(row) else '1x'
        # Tries to extrac the dilation factor.
        f = 1.
        try:
            f = float(dilation.split('x')[0])
            assert f >= 1.
        except:
            pass
        dilation_factors[email] = f
    print(dilation_factors)

    # Figures out for each student the last valid submission.
    leniency = datetime.timedelta(minutes=args.leniency)
    last_submission = {}
    for row in values:
        email_idx = ord(args.email_column) - ord('A')
        timestamp_idx = ord(args.timestamp_column) - ord('A')
        url_idx = ord(args.file_column) - ord('A')
        email = row[email_idx]
        timestamp = datetime.datetime.strptime(row[timestamp_idx], "%m/%d/%Y %H:%M:%S")
        url = row[url_idx]
        # Are we still on time?
        deadline = normal_deadline + leniency + datetime.timedelta(hours=(dilation_factors.get(email, 1.) - 1.) * args.duration)
        if deadline > timestamp:
            # Yes we are on time.
            last_submission[email] = url
        else:
            print("The submission by %s at %r was rejected as late" % (email, timestamp))

    # Now downloads all the last submissions.
    for email, url in last_submission.items():
        docid = url.split("=")[-1]
        if args.nbgrader:
            dn = os.path.join(args.destination_dir, 'submitted', email, args.assignment)
            fn = os.path.join(dn, args.notebook)
        else:
            dn = args.destination_dir
            fn = os.path.join(dn, email + '.' + args.extension)
        # Creates the directory if necessary.
        if not os.path.exists(dn):
            os.makedirs(dn)
        with open(fn, 'wb') as f:
            request = drive_service.files().get_media(fileId=docid)
            downloader = MediaIoBaseDownload(f, request)
            done = False
            while done is False:
                status, done = downloader.next_chunk()
                print("Download %s to %s : %d%%." % (email, fn, int(status.progress() * 100)))
    # Finally, writes the csv file with all the students who have submitted,
    # so their work can be included.
    csv_fn = 'students_' + datetime.datetime.utcnow().isoformat().replace(':', '-').replace('.','-') + '.csv'
    with open(csv_fn, 'w', newline='') as csvfile:
        fieldnames=['id', 'email']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for email in last_submission.keys():
            writer.writerow({'id': email, 'email': email})


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--spreadsheet_id', default=None,
                        help="ID of the spreadsheet to use as base of download.")
    parser.add_argument('--drc_spreadsheet_id', default=None,
                        help="ID of spreadsheet containing the time dilation")
    parser.add_argument('--drc_column', default=None,
                        help='Column in DRC spreadsheet specifying how much extra time the student gets. '
                        'The information is in the format fx, where f is a floating point number, '
                        'e.g., 1.5 .  If this is missing, it is assumed no dilation.')
    parser.add_argument('--deadline', default=None,
                        help='Deadline in format YYYY-MM-DD HH:MM')
    parser.add_argument('--leniency', type=float, default=5.,
                        help="Submissions that are fewer than this number of minutes late will be accepted.")
    parser.add_argument('--duration', type=float, default=None,
                        help='Duration, in hours')
    parser.add_argument('--timestamp_column', default='A',
                        help='Column containing the timestamp.')
    parser.add_argument('-m', '--email_column', default='B',
                        help='Column containing email')
    parser.add_argument('--drc_email_column', default='B',
                        help="Column in DRC spreadsheet where the student email is found.")
    parser.add_argument('-f', '--file_column', default='C',
                        help='Column containing submission')
    parser.add_argument('--sheet', type=str, default='Form Responses 1',
                        help='Sheet name')
    parser.add_argument('--drc_sheet', type=str, default='Form Responses 1',
                        help='DRC sheet name')
    parser.add_argument('-n', '--nbgrader', action='store_true', default=False,
                        help='Use nbgrader format to download the files.'
                        )
    parser.add_argument('-o', '--notebook', type=str, default='assignment.ipynb',
                        help='Name of ipython notebook used for the test. '
                        'This option is only used for nbgrader.'
                        )
    parser.add_argument('-e', '--extension', type=str, default='w2p',
                        help='Extension to be used in download (not necessary if the '
                        'nbgrader option is also specified.'
                        )
    parser.add_argument('-c', '--destination_dir', type=str,
                        default='.', help='Directory where to store the submissions. '
                        'For nbgrader, this should be the class directory.')
    parser.add_argument('-a', '--assignment', type=str, default=hex(random.getrandbits(64))[2:],
                        help='Assignment name (defaults to a random string)')
    args = parser.parse_args()
    main(args)