
Author: Luca de Alfaro, luca@ucsc.edu

License: BSD

# Overview

These tools enable you to distribute, collect, grade, and share feedback on homework consisting of Python notebooks run on Google Colab. 

# Installation

## Google Cloud

For reference, here is an [overall guide to building projects that interact with Google Drive](https://developers.google.com/workspace/guides/get-started).

### Creating a Google Cloud Project

First, you need to [create a Google Cloud Project](https://developers.google.com/workspace/guides/create-project) as part of your @ucsc.edu account or your institutional account if different. 

### Enabling the sheets and drive APIs

Next, you need to enable:

* [The Sheets api](https://developers.google.com/sheets/api/quickstart/python)
* [The Drive api](https://console.developers.google.com/apis/library/drive.googleapis.com)

To do so, go to _APIs & Services > Library_ in your Google Project, and select these APIs, and enable them.

### Creating credentials

Next, you need to create credentials to allow your code to interact with Drive and Sheets.  [Follow this guide](https://developers.google.com/workspace/guides/create-credentials), and: 

* Select an internal application in the Oauth screen. 
* You can enter Quickstart in the app name. 
* You can follow the instructions at point 8 for a Quickstart. 
* Download the credentials in a file called `credentials.json` here in this directory. 

### Installing the requirements. 

First, create a new environment where to work: 

    conda create -n exams python=3.7
    conda activate exams

You need to install the requirements. 

    pip install -r requirements.txt

And you need to install extensions for nbgrader:

    jupyter nbextension install --sys-prefix --py nbgrader --overwrite
    jupyter nbextension enable --sys-prefix --py nbgrader
    jupyter serverextension enable --sys-prefix --py nbgrader

# Creating a Class or Exam 

## Creating the nbgrader class

For an exam, proceed in the same way.  Go to a directory of your liking, and do: 

    nbgrader quickstart class_name
    
Then, change the following configurations in nbgrader_config.py. 
First, 

    cd class_name

Then change these lines in `nbgrader_config.py` as follows (uncommenting these lines, of course):

    c.ClearSolutions.code_stub = {'python': '# YOUR CODE HERE'}
    c.ExecutePreprocessor.timeout = 60  # Uncomment this line! 
    c.Exchange.root = '/Users/luca/Teaching/nb/srv/nbgrader/exchange'
    
The latter directory needs to be simply a directory where ... you can put stuff you don't care about.  But _it must exist!_ 

# Configuration

## Drive folders

It is necessary to create two drive folders:

* **A folder for homework assignments.**  This needs to be shared in _read_ mode with most graders, but in _edit_ mode with the trusted TAs that need to be able to check the revision histories of the notebooks.

* **A folder for homework feedback.** This can be shared in write mode with TAs and graders, so that they can run and edit the code of students. 

In the folder for homework assingments, each student has a folder, so that the homework of student a@example.com can be found there.  The folder is organized as follows: 

- MyClass
  - MyClass a@ucsc.edu
    - Assignment 1 for student a@ucsc.edu
    - Assignment 2 for student a@ucsc.edu
    - ... 
  - MyClass b@ucsc.edu
    - ... 
  - MyClass c@ucsc.edu
    - ... 

## settings.yaml

You then need to create the settings.yaml file; you can use settings_sample.yaml as an example.  In it, you need to specify, among others: 

* The directory on your laptop where the nbgrader class is.
* The folder IDs on google drive of the above folders, for assignments and for feedback. 
* The ID, sheet name, and column of the spreadsheet containing the email list of all students.  This is used to distribute to each student their own homework.  


# Starting nbgrader

At this point, you are ready to start nbgrader. 

    cd <class_directory>
    jupyter-notebook

### Creating an assignment

Then, click on the FormGrader tab.  And click on Add new assignment.
Click on the name of the new assignment, then click on upload, 
and upload the <notebook>_test.ipynb you have created using `prepare_notebook.py`. 

### Editing an assignment and choosing the number of points

The test will be in source format and still contain the `### BEGIN SOLUTION` 
and `### END SOLUTION` tags in it.  

Click on View > Cell Toolbar and select the "Create an Assignment" toolbar. 
That will give you access to the tools for creating an assignment.  
Thanks to `prepare_assignment.py`, cells should already be labeled each in the
correct way, but you may want to check. You can edit it as much as you want. 

### Creating the assignment for distribution

Click on the magic gradute cap in the assignment tab, and that will generate the
assignment that is ready to be released.

# Assigning the exercise set

## Preparing the exercise set for CSE 30

To prepare a notebook for CSE 30, you can take one of the _source_ notebooks (that contain the solutions) and do: 

    python prepare_notebook.py path/to/source/notebook.ipynb

and this will generate two notebooks as output: 

* the _chapter_, which contains the notebook minus the solutions, which you can post online as study material, and 

* the _test_, which you can copy to the assignments folder in nbgrader and adapt to be the test for your students. 

## Assigning an exercise set

The simplest way consists in uploading the _released_ notebook generated above (in the `/release` directory of nbgrader) to Colab, and share with the students a read-only link to that notebook.  Instruct the students to make a copy of the notebook, and work on their own copy. 

The better way consists in creating a notebook per student, like this: 

    python assign_homework.py -a <homework> 

This enables each student to work on their own copy.  The benefits to the instructor of doing this are several: 

* You can quickly go see what each student is doing, e.g., to help them in office hours. 
* You can check the revision history of each assignment, e.g., to check that the student did the work before the deadline in case the student forgot to submit (it happens). 

# Grading a test

Download the submissions from the form where they have been submitted:

    python download_notebooks.py -s <spreadsheet_id> --sheet Submissions -a homework1

where: 

* <spreadsheet_id> is the id on drive of the spreadsheet containing the student submissions.  For example, if the URL of the google sheets looks like `https://docs.google.com/spreadsheets/d/someid/edit#gid=somenumber` then the ID is `someid`. 
* Submissions is the name of the sheet in the spreadsheet where the submmissions are (see python download_notebooks.py --help for more options, e.g., for declaring the column where the file URL and where the student email are)
* homework1 is the name of the homework created in nbgrader

Besides downloading the notebooks, this also creates a file `students_<datetime>.csv` containing 
the emails of the students.  You need to add these students to the database contained in nbgrader. To do so, go to the nbgrader class directory, and give the command:

    nbgrader db student import <path to above csv file>

Then, again from the nbgrader class directory:

    nbgrader autograde homework1

Some of the grading might fail, due to incorrect metadata in the notebook.  You can try to fix assignments that failed automatically via: 

    python fix_submissions.py -a homework1

Get the grades, via: 

    python get_grades.py -a homework1
    
which generates a local .csv file with the grades (you can import it into a Google spreadsheet if you wish).  Finally, you can share the feedback with:

    python upload_feedback.py -a homework1

----
