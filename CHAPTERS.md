# Generating Homework from Chapters

## The source Colab notebook

Homework assignments are produced from Google Colab notebooks that obey special conventions,
outlined below.  All _source_ book chapters from the textbook _Abstractions In Python_ obey 
these conventions. 

The Colab notebooks must contain cells of the following types. 

#### 1. Text cells
You can use text (markup) cells. 

#### 2. Code cells
These are read-only code cells that do not contain questions 
(places where the students must write code) nor tests. 
You can simply write them. 

#### 3. Question cells

These are the code cells where the students must include their answers. 
These cells must contain

    ### BEGIN SOLUTION
    ...
    ### END SOLUTION
    
as a marker for where the student answer goes. 
You must fill in the answer (you cannot leave it blank).
The cells also must be forms, that is, start with a form title: 

    #@title Exercise: name of the exercise
    
This will allow you to "fold closed" the cell in Colab, preventing the solution
to be displayed to the students while you teach. 

#### 4. Test cells

These are the code cells containing tests.  I use `nose` for tests, but `assert` 
also works.  These cells _must_ contain 

    ### BEGIN HIDDEN TESTS
    ...
    ### END HIDDEN TESTS
    
The line above are _always_ needed.  If you do not wish to have hidden tests,
you can put a `pass` as hidden test.  Hidden tests are useful to prevent
students from hard-coding the answers to the tests. 
These cells _must_ be also forms, beginning with the line: 

    #@title Tests for the exercise
    
In this way, you can prevent showing the hidden tests to the students. 
In any case, again, you need the BEGIN/END HIDDEN TESTS lines in _every_ test 
cell. 

### Producing the source exercise set

To produce the source exercise set, first download the Colab notebook as a 
`.ipynb` notebook, and then use the command:

    python prepare_notebook.py <notebook.ipynb>
    
This will produce two files: 

* `notebook_chapter.ipynb` is a book chapter, that contains neither the solutions to
the exercises, nor the hidden tests.  This chapter is suitable to be posted to the
class web page (you can re-upload it in Colab and re-share it, for instance).

* `notebook_test.ipynb` is the test source, which you now will tranform in a 
released test as described below. 

## Producing the exercise set

Once you have `notebook_test.ipynb`, you must: 

* Create an assignment in nbgrader
* Upload `notebook_test.ipynb` to that assignment. 
* Edit `notebook_test.ipynb` in nbgrader, using the _Prepare an Assignment_ toolbar.  In particular, you must: 
    * Assign the proper number of points to each problem (the default is 10; you can edit it).
    * Insert instructions at the top of the notebook.
    * Insert, at the top of the notebook, the link to the Google form the studens need to use to submit the assignment.  Explain to the students that they must first download the notebook as an .ipynb, then upload it to that Google form. 
* In nbgrader, click on the Generate button, to generate the released assignment. 
