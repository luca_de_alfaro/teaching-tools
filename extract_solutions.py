# Copyright Luca de Alfaro, 2019
# BSD License

import argparse
import json
import os
import yaml


class SubmittedNotebook(object):

    def __init__(self, notebook, args):
        self.notebook = notebook
        self.args = args

    def extract_code(self):
        """Extracts the code from the solution cells."""
        code = []
        for input_cell in self.notebook['cells']:
            try:
                if input_cell.get('cell_type') == 'code':
                    if input_cell['metadata']['nbgrader']['solution']:
                        code.append("# id: " + str(input_cell['metadata'].get('id')) + '\n')
                        code.extend(input_cell['source'])
                        code.extend(['\n', '\n'])
            except:
                pass
        return code


def main(args):
    with open(args.settings, 'r') as f:
        settings = yaml.load(f, Loader=yaml.SafeLoader)

    # Creates the directory where to put the files.
    cdir = os.path.join(settings['class_path'], 'submitted_code', args.assignment)
    if not os.path.exists(cdir):
        os.makedirs(cdir)

    # Converts the submitted notebook to submitted code.
    submission_dir = os.path.join(settings['class_path'], 'submitted')
    emails = [e for e in os.listdir(submission_dir) if not e.startswith('.')]
    emails.sort()
    for email in emails:
        student_dir = os.path.join(submission_dir, email, args.assignment)
        if os.path.exists(student_dir):
            with os.scandir(student_dir) as it:
                for entry in it:
                    if entry.is_file() and entry.name.endswith('.ipynb'):
                        # We extract the code.
                        code_name = email + "_" + '.'.join(entry.name.split('.')[:-1] + ['py'])
                        code_path = os.path.join(cdir, code_name)
                        with open(entry.path) as in_f:
                            try:
                                notebook = json.load(in_f)
                                nb = SubmittedNotebook(notebook, args)
                                with open(code_path, 'w') as out_f:
                                    out_f.write(''.join(nb.extract_code()))
                            except:
                                print("--- Error processing submission for", email)
        print("Processed submission by", email)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--assignment', type=str, default=None,
                        help='Assignment for which to extract the work')
    # The default is fine.
    parser.add_argument('--settings', type=str, default='settings.yaml',
                        help='Settings file to be used')
    args = parser.parse_args()
    main(args)
