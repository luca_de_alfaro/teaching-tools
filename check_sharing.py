# Copyright Luca de Alfaro, 2022.
# BSD License.

import argparse
import csv
import datetime
import os
import pickle
import yaml

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.http import MediaFileUpload, MediaIoBaseUpload

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets',
          'https://www.googleapis.com/auth/drive']

class ShareAnalysis(object):
    
    def __init__(self, drive_service, share_folder_id, ok_emails=[]):
        """Initializes a file distributor."""
        self.drive_service = drive_service
        self.share_folder_id = share_folder_id
        self.ok_emails = [None] + ok_emails
        # Mapping from students, to their folders.
        self.email_to_folder_id = {}
        self._read_class_folder()

    def _read_class_folder(self):
        """Reads the base folder, constructing a mapping from
        emails of students, to folder_ids for their folders."""
        # Lists the folders in the given folder.
        q = "'%s' in parents" % self.share_folder_id
        q += " and mimeType = 'application/vnd.google-apps.folder'"
        page_token = None
        while True:
            response = self.drive_service.files().list(
                q=q,
                spaces='drive',
                fields='nextPageToken, files(id, name)',
                pageToken=page_token).execute()
            for file in response.get('files', []):
                email = file.get('name', '').split()[-1]
                self.email_to_folder_id[email] = file.get('id')
            page_token = response.get('nextPageToken', None)
            if page_token is None:
                break

    def _read_student_files(self, folder_id):
        """Reads the files of a given student, returning a dictionary mapping
        file name to file permissions"""
        q = "'%s' in parents" % folder_id
        q += " and mimeType = 'application/vnd.google.colaboratory'"
        page_token = None
        name_to_permissions = {}
        while True:
            response = self.drive_service.files().list(
                q=q,
                spaces='drive',
                fields='nextPageToken, files(id, name, permissions)',
                pageToken=page_token).execute()
            for file in response.get('files', []):
                name = file.get('name', '')
                permissions = file.get('permissions')
                name_to_permissions[name] = permissions
            page_token = response.get('nextPageToken', None)
            if page_token is None:
                break
        return name_to_permissions
        
    def analyze(self):
        self._read_class_folder()
        share_with = []
        for email, folder_id in self.email_to_folder_id.items():
            print("Analyzing %s" % email)
            this_ok_emails = [email] + self.ok_emails
            name_to_permissions = self._read_student_files(folder_id)
            for name, permissions in name_to_permissions.items():
                for p in permissions:
                    if p.get('type') == 'user' and p.get('emailAddress') not in this_ok_emails:
                        share_with.append(
                            dict(email=email, name=name, shared=p.get('emailAddress')))
                        print("%s shared %s with %s" % (email, name, p.get('emailAddress')))
        return share_with
    
def main(args):
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            try:
                creds.refresh(Request())
            except:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server()
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    drive_service = build('drive', 'v3', credentials=creds)
    sheet_service = build('sheets', 'v4', credentials=creds)
    
    with open(args.settings, 'r') as f:
        settings = yaml.load(f, Loader=yaml.SafeLoader)

    # Creates the share analyzer.
    ok_emails = args.ok_emails.split(',')
    share_folder_id = args.share_folder_id or settings['share_folder_id']
    share_analysis = ShareAnalysis(drive_service, share_folder_id, ok_emails=ok_emails)
    share_with = share_analysis.analyze()
    csv_fn = 'share_' + datetime.datetime.now().isoformat().replace(':', '-').replace('.','-') + '.csv'
    with open(csv_fn, 'w', newline='') as csvfile:
        fieldnames=['email', 'name', 'shared']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for info in share_with:
            writer.writerow(info)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--settings', type=str, default='settings.yaml',
                    help='Settings file to be used')
    parser.add_argument('--share_folder_id', type=str, default=None, 
                        help='The folder id of the class.')
    parser.add_argument('--ok_emails', type=str, default="",
                        help='Comma separated list of emails that are ok to share with.')
    args = parser.parse_args()
    main(args)
