# Copyright Luca de Alfaro, 2019
# BSD License

import argparse
import csv
from collections import defaultdict
import datetime
import os
import pickle
import random

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.http import MediaIoBaseDownload

import notebook_utils

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly',
            'https://www.googleapis.com/auth/drive.readonly']
# https://www.googleapis.com/auth/spreadsheets for r/w
#


def main(args):
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            try:
                creds.refresh(Request())
            except:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server()
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    sheet_service = build('sheets', 'v4', credentials=creds)
    drive_service = build('drive', 'v3', credentials=creds)

    # Call the Sheets API
    range_name = args.sheet + '!' + args.email_column + '2:' + args.file_column
    sheet = sheet_service.spreadsheets()
    result = sheet.values().get(spreadsheetId=args.spreadsheet_id,
                                range=range_name).execute()
    values = result.get('values', [])

    if not values:
        print('No data found.')
        return

    # Now downloads all the submissions.
    student_submissions = {row[0]: row[-1] for row in values}
    wrong_watermarks = []
    watermark_to_email = defaultdict(list)
    for email, url in student_submissions.items():
        if args.test:
            print(email, url)
            continue
        docid = url.split("=")[-1]
        if args.nbgrader:
            dn = os.path.join(args.destination_dir, 'submitted', email, args.assignment)
            fn = os.path.join(dn, args.notebook)
        else:
            dn = args.destination_dir
            fn = os.path.join(dn, email + '.' + args.extension)
        # Creates the directory if necessary.
        if not os.path.exists(dn):
            os.makedirs(dn)
        with open(fn, 'wb') as f:
            request = drive_service.files().get_media(fileId=docid)
            downloader = MediaIoBaseDownload(f, request)
            done = False
            while done is False:
                status, done = downloader.next_chunk()
                print("Download %s to %s : %d%%." % (email, fn, int(status.progress() * 100)))
        # Checks the watermark, if desired.
        if args.watermark is not None:
            valid, sig = notebook_utils.check_watermark(fn, email, key=args.watermark)
            if not valid:
                wrong_watermarks.append((email, sig))
            watermark_to_email[sig].append(email)
    # Writes data on wrong watermarks.
    watermark_fn = 'watermarks_' + datetime.datetime.now().isoformat().replace(':', '-').replace('.','-') + '.csv'
    with open(watermark_fn, 'w', newline='') as wrong_watermark_file:
        fieldnames = ['email', 'matches']
        writer = csv.DictWriter(wrong_watermark_file, fieldnames=fieldnames)
        writer.writeheader()
        for email, sig in wrong_watermarks:
            print("%s has wrong watermark, shared with %r" % (email, watermark_to_email[sig]))
            writer.writerow({'email': email, 'matches': ','.join(watermark_to_email[sig])})
    # Finally, writes the csv file with all the students who have submitted,
    # so their work can be included.
    csv_fn = 'students_' + datetime.datetime.now().isoformat().replace(':', '-').replace('.','-') + '.csv'
    with open(csv_fn, 'w', newline='') as csvfile:
        fieldnames=['id', 'email']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for email in student_submissions.keys():
            writer.writerow({'id': email, 'email': email})


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--settings', type=str, default='settings.yaml',
                        help='Settings file to be used')
    parser.add_argument('-s', '--spreadsheet_id', default=None,
                        help="ID of the spreadsheet to use as base of download.")
    parser.add_argument('-m', '--email_column', default='B',
                        help='Column containing email')
    parser.add_argument('-f', '--file_column', default='C',
                        help='Column containing submission')
    parser.add_argument('--sheet', type=str, default='Form Responses 1',
                        help='Sheet name')
    parser.add_argument('-n', '--nbgrader', action='store_true', default=False,
                        help='Use nbgrader format to download the files.'
                        )
    parser.add_argument('-o', '--notebook', type=str, default='assignment.ipynb',
                        help='Name of ipython notebook used for the test. '
                        'This option is only used for nbgrader.'
                        )
    parser.add_argument('-e', '--extension', type=str, default='w2p',
                        help='Extension to be used in download (not necessary if the '
                        'nbgrader option is also specified.'
                        )
    parser.add_argument('-c', '--destination_dir', type=str,
                        default='.', help='Directory where to store the submissions. '
                        'For nbgrader, this should be the class directory.')
    parser.add_argument('-a', '--assignment', type=str, default=hex(random.getrandbits(64))[2:],
                        help='Assignment name (defaults to a random string)')
    parser.add_argument('-t', '--test', action='store_true', default=False,
                        help="Test but do not download anything")
    parser.add_argument('--watermark', type=str, default=None,
                        help="Watermark key.  If specified, assignments are watermarked.")
    args = parser.parse_args()
    main(args)