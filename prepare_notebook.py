# Copyright Luca de Alfaro, 2019
# BSD License

import argparse
import json
import os

from notebook_utils import Notebook


def main(args):
    assert args.input_fn is not None
    in_name, in_ext = os.path.splitext(args.input_fn)

    # Converts the input notebook to a chapter.
    chapter_output_fn = in_name + "_chapter" + in_ext
    with open(args.input_fn, 'r') as in_f:
        notebook = json.load(in_f)
    _, new_name = os.path.split(chapter_output_fn)
    args.chapter = True
    nb = Notebook(notebook, args)
    nb.convert_notebook(new_name)
    print("Creating", chapter_output_fn)
    with open(chapter_output_fn, mode='w', newline='') as out_f:
        json.dump(nb.get_result(), out_f)

    # Converts the input notebook to a test.
    test_output_fn = in_name + "_test" + in_ext
    with open(args.input_fn, 'r') as in_f:
        notebook = json.load(in_f)
    _, new_name = os.path.split(test_output_fn)
    args.chapter = False
    nb = Notebook(notebook, args)
    nb.convert_notebook(new_name)
    print("Creating", test_output_fn)
    with open(test_output_fn, mode='w', newline='') as out_f:
        json.dump(nb.get_result(), out_f)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_fn', type=str, default=None,
                        help='Name of input notebook')
    parser.add_argument('-q', '--preserve_questions', action='store_true',
                        default='False',
                        help='Keep the questions with code in the chapters')
    args = parser.parse_args()
    main(args)
