# Copyright Luca de Alfaro, 2019
# BSD License

import argparse
import json
import os
import pickle
import yaml
from pathlib import Path

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

from notebook_utils import Notebook
from file_distributor import FileDistributor

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets',
          'https://www.googleapis.com/auth/drive']
# https://www.googleapis.com/auth/spreadsheets for r/w


def main(args):
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            try:
                creds.refresh(Request())
            except:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server()
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    drive_service = build('drive', 'v3', credentials=creds)

    with open(args.settings, 'r') as f:
        settings = yaml.load(f, Loader=yaml.SafeLoader)

    # Creates the file distributor.
    file_distributor = FileDistributor(drive_service,
                                       settings['class_name'] + " " + "Feedback",
                                       settings['feedback_folder_id'])

    # Now uploads each piece of feedback.
    graded_dir = os.path.join(settings['class_path'], 'autograded')
    feedback_dir = os.path.join(settings['class_path'], 'feedback')
    emails = [e for e in os.listdir(graded_dir) if not e.startswith('.')]
    if args.email is not None:
        filter_emails = args.email.split(",")
        emails = list(set(emails) & set(filter_emails))
    emails.sort()
    
    # Takes care of the resume argument.
    if args.resume is not None and args.resume in emails:
        emails = emails[emails.index(args.resume) + 1:]

    for email in emails:
        student_graded_dir = os.path.join(graded_dir, email, args.assignment)
        student_feedback_dir = os.path.join(feedback_dir, email, args.assignment)
        if os.path.exists(student_graded_dir):
            Path(student_feedback_dir).mkdir(parents=True, exist_ok=True)
            with os.scandir(student_graded_dir) as it:
                for entry in it:
                    if entry.is_file() and entry.name.endswith('.ipynb'):
                        # We add the grade information to the file.
                        student_fn = os.path.join(student_graded_dir, entry.name)
                        with open(student_fn, 'r') as in_f:
                            notebook = json.load(in_f)
                        new_name = '.'.join(entry.name.split('.')[:-1]) + "_feedback"
                        nb = Notebook(notebook, args)
                        nb.add_feedback(new_name)
                        out_name = os.path.join(student_feedback_dir, new_name + ".ipynb")
                        with open(out_name, mode="w", newline='') as out_f:
                            json.dump(nb.get_result(), out_f)
                        # Uploads the file.
                        file_distributor.distribute_file(
                            email, out_name, new_name,
                            'application/vnd.google.colaboratory',
                            update=args.update)
                        print("Created feedback for", email)
    print("All done")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # Needed.
    parser.add_argument('-a', '--assignment', type=str, default=None,
                        help='Assignment for which to give feedback')
    parser.add_argument('-u', '--update', action='store_true', default=True,
                        help='Updates existing feedback if it exists.')
    parser.add_argument('--email', type=str, default=None,
                        help="If specified, only uploads this feedback.")
    parser.add_argument('-r', '--resume', type=str, default=None,
                    help='Resume after this student')

    # The default is fine.
    parser.add_argument('--settings', type=str, default='settings.yaml',
                        help='Settings file to be used')
    parser.add_argument('--retry', type=str, default=None,
                        help='Student from which to retry, if any')
    args = parser.parse_args()
    main(args)
