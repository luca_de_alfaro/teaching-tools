# Copyright Luca de Alfaro, 2019
# BSD License

import argparse

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly',
            'https://www.googleapis.com/auth/drive.readonly']
# https://www.googleapis.com/auth/spreadsheets for r/w
#


def main(args):
    print("""To test auth, ou need to enable: 
    * The sheets api: https://developers.google.com/sheets/api/quickstart/python
    * The drive api: https://console.developers.google.com/apis/library/drive.googleapis.com
""")
    creds = None
    flow = InstalledAppFlow.from_client_secrets_file('credentials.json', SCOPES)
    creds = flow.run_local_server()
    sheet_service = build('sheets', 'v4', credentials=creds)
    drive_service = build('drive', 'v3', credentials=creds)
    print("Success!")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    args = parser.parse_args()
    main(args)