# Copyright Luca de Alfaro, 2020.
# BSD License.

import json
import datetime
import hashlib
import random
import traceback

MAY_BE_QUESTION = 'SOLUTION'
MAY_BE_TEST = 'TESTS'
QUESTION_MARKUP = '### BEGIN SOLUTION'
QUESTION_MARKUP_END = '### END SOLUTION'
TEST_MARKUP = '### BEGIN HIDDEN TESTS'
TEST_MARKUP_END = '### END HIDDEN TESTS'
FORM_MARKUP = '#@title' # Used for hiding information.
FORM_TITLE = '@title'
RAISE_UNIMPLEMENTED = 'raise NotImplementedError()'
DATE_TEXT = """
Prepared on: %s

This is a book chapter; it is not a homework assignment.  
Do not submit it as a solution to a homework assignment; you would receive no credit.
"""
GRADE_COMMENT = "### For this question, you received {earned_points} out of {total_points} points.\n"
TOTAL_SCORE = "You received {earned_points} out of {total_points} points.\n"

class CannotImportMetadata(Exception):
    pass

class SuspiciousCell(Exception):
    pass

class Cell(dict):

    def __init__(self, d, args):
        super().__init__(d)
        self.args = args
        self.keep = True # Keep in modified notebook.

    def __repr__(self):
        return '\n'.join(self.get('source', []))

    def is_cell_type(self, markup):
        """Returns True/False depending on whether a cell contains a given markup."""
        for code_line in self.get('source', []):
            if markup in code_line:
                return True
        return False

    def is_cell_question(self):
        return self.is_cell_type(QUESTION_MARKUP)

    def is_cell_test(self):
        return self.is_cell_type(TEST_MARKUP)

    def is_cell_form(self):
        """Forms are used to hide information."""
        if len(self.get('source', [])) < 1:
            return False
        return self['source'][0].startswith(FORM_MARKUP)

    def is_error(self):
        """Determine if an error occurred."""
        if self['cell_type'] != 'code':
            return False
        # From nbgrader/utils.py
        for output in self["outputs"]:
            # option 1: error, return 0
            if (output['output_type'] == 'error' or
                    (output['output_type'] == "stream" and
                     output['name'] == "stderr")):
                return True
        return False

    def get_grade(self):
        """Returns None if this cell was not graded, and otherwise,
        returns a dictionary with keys:
        - total_points
        - earned_points
        the latter is either 0, or the number of points, depending on whether
        there were errors.  This is used to generate feedback."""
        if self['cell_type'] != 'code':
            return None
        if self.get('metadata') is None:
            return None
        if self['metadata'].get('nbgrader') is None:
            return None
        if self['metadata']['nbgrader'].get('grade') is not True:
            return None
        # This is a grade cell.
        total_points = int(self['metadata']['nbgrader'].get('points', 0))
        is_error = self.is_error()
        earned_points = 0 if is_error else total_points
        return dict(total_points=total_points, earned_points=earned_points)

    def get_and_add_grade(self):
        """Returns the same as get_grade, and modifies the cell adding
        an initial comment with the number of points awarded."""
        outcome = self.get_grade()
        if outcome is not None:
            line = GRADE_COMMENT.format(**outcome)
            self["source"] = [line, "\n"] + self["source"]
        return outcome

    def remove_code(self, start_markup, end_markup,
                    leave_markers=False, insert_code=None):
        """Removes code from a start to and end markup markers,
        optionally replacing the removed code with a new piece of code.
        Markers can be left in place, or removed as well."""
        new_code = []
        inside = False
        for code_line in self['source']:
            if inside:
                if end_markup in code_line:
                    # We are exiting.
                    inside = False
                    if leave_markers:
                        new_code.append(code_line)
            else:
                # We are outside.
                if start_markup in code_line:
                    # We are entering.
                    inside = True
                    if leave_markers:
                        new_code.append(code_line)
                    if insert_code is not None:
                        # Inserts the new code, properly justified.
                        leading_spaces = len(code_line) - len(code_line.lstrip(" "))
                        new_line = " " * leading_spaces + insert_code + '\n'
                        new_code.append(new_line)
                else:
                    # We are still outside.
                    new_code.append(code_line)
        self['source'] = new_code

    def transform_cell(self):
        if self['cell_type'] == 'markdown':
            self.transform_markup_cell()
        elif self['cell_type'] == 'code':
            self.transform_code_cell()
            # Sanity checks
            for line in self['source']:
                assert 'START' not in line, "Error: %r" % self['source']
                if self.args.chapter:
                    assert 'HIDDEN' not in line, "Error: %r" % self['source']

    def transform_markup_cell(self):
        if not self.args.chapter:
            self['metadata'] = dict(nbgrader=dict(
                grade=False,
                locked=True,
                solution=False,
                schema_version=1,
                grade_id='cell-%x' % random.getrandbits(64)
            ))

    def transform_question_cell(self):
        """Transforms a cell that contains user input."""
        if self.args.chapter:
            # We have to eliminate the code segment.
            self.remove_code(QUESTION_MARKUP, QUESTION_MARKUP_END,
                             leave_markers=True,
                             insert_code=RAISE_UNIMPLEMENTED)
        else:
            # We are preparing a notebook.
            # We have to ensure that the cell will be labeled
            # in the appropriate way by nbgrader.
            self['metadata'] = dict(nbgrader=dict(
                grade=False,
                locked=False,
                solution=True,
                schema_version=1,
                grade_id='cell-%x' % random.getrandbits(64)
            ))

    def transform_test_cell(self):
        """Transform a cell containing a test"""
        if self.args.chapter:
            # We remove the hidden tests.
            self.remove_code(TEST_MARKUP, TEST_MARKUP_END,
                             leave_markers=False)
        else:
            # We ensure the tests are properly labeled in nbgrader.
            self['metadata'] = dict(nbgrader=dict(
                grade=True,
                points=10, # Just a placeholder for fine tuning.
                locked=True,
                solution=False,
                schema_version=1,
                grade_id='cell-%x' % random.getrandbits(64)
            ))

    def transform_normal_code_cell(self):
        """This is a read-only cell containing normal code."""
        if not self.args.chapter:
            self['metadata'] = dict(nbgrader=dict(
                grade=False,
                locked=True,
                solution=False,
                schema_version=1,
                grade_id='cell-%x' % random.getrandbits(64)
            ))

    def remove_form_markings(self):
        """Removes the cell markings of a form cell."""
        # First, removes the view attribute.
        if 'cellView' in self['metadata']:
            del self['metadata']['cellView']
        if self.args.preserve_questions:
            # Transforms the form title into a normal title.
            assert len(self['source']) > 0
            self['source'][0] = '##' + self['source'][0].replace(FORM_TITLE, '')
        else:
            # We remove the entire question/answer.
            self.keep = False

    def validate_code_cell(self):
        """Checks that the cell is not likely to be a mistake."""
        if self.is_cell_type(MAY_BE_QUESTION):
            if not (self.is_cell_type(QUESTION_MARKUP) and
                    self.is_cell_type(QUESTION_MARKUP_END)):
                raise SuspiciousCell(repr(self))
        if self.is_cell_type(MAY_BE_TEST):
            if not (self.is_cell_type(TEST_MARKUP) and
                    self.is_cell_type(TEST_MARKUP_END)):
                raise SuspiciousCell(repr(self))

    def remove_end_space(self):
        """Remove the space from the end of the lines, as they
        generate problems in colab."""
        # First removes the end space...
        self['source'] = [line.rstrip() for line in self['source']]
        # ... then reinserts the final \n
        self['source'] = [line + '\n' for line in self['source']]

    def transform_code_cell(self):
        # Code cell.
        self.remove_end_space()
        self.validate_code_cell()
        # If this is a homework, clears the outputs.
        if not self.args.chapter:
            self['outputs'] = []
        # If the cell is a form, we must first of all remove the form markings.
        if self.is_cell_form() and (
            self.is_cell_question() or self.is_cell_test()):
            self.remove_form_markings()
        # Then, we take the resulting cell, and we transform it.
        if self.is_cell_question():
            self.transform_question_cell()
        elif self.is_cell_test():
            self.transform_test_cell()
        else:
            self.transform_normal_code_cell()
        # Final adjustements, to remove some colab information if present.
        if 'metadata' in self:
            mt = self['metadata']
            if 'executionInfo' in mt:
                del mt['executionInfo']
            if 'outputId' in mt:
                del mt['outputId']
            if 'colab' in mt:
                del mt['colab']

    def get_result(self):
        """Gets the result of the transformation as a plain old dict."""
        return dict(self)


class Notebook(dict):

    def __init__(self, notebook, args):
        super().__init__(notebook)
        self.args = args

    def prepare_date_cell(self):
        date_string = datetime.datetime.now().strftime("%c")
        source_text = DATE_TEXT % date_string
        date_cell = {
                "cell_type": "markdown",
                "metadata": {
                    "id": '%x' % random.getrandbits(64),
                    "colab_type": "text"
                },
                "source": source_text
            }
        return date_cell

    def convert_notebook(self, new_name):
        # Transforms the cells.
        cells = []
        for input_cell in self['cells']:
            # Generates the output cell.
            cell = Cell(input_cell, self.args)
            cell.transform_cell()
            if cell.keep:
                cells.append(cell.get_result())
        # Adds the disclaimer if needed.
        if self.args.chapter:
            cells.insert(1, self.prepare_date_cell())
        self['cells'] = cells
        # Fixes the new name.
        if 'metadata' in self and 'colab' in self['metadata']:
            cm = self['metadata']['colab']
            if 'name' in cm:
                cm['name'] = new_name

    def add_feedback(self, new_name):
        """Adds feedback to a graded notebook."""
        total_points = 0
        earned_points = 0
        # Adds feedback to the cells.
        cells = []
        for input_cell in self['cells']:
            # Generates the output cell.
            cell = Cell(input_cell, self.args)
            d = cell.get_and_add_grade()
            if d is not None:
                total_points += d['total_points']
                earned_points += d['earned_points']
            cells.append(cell.get_result())
        if total_points > 0:
            cells.insert(0, {
                "cell_type": "markdown",
                "metadata": {
                    "id": '%x' % random.getrandbits(64)
                },
                "source": [
                    "# Overall Score\n",
                    "\n",
                    TOTAL_SCORE.format(total_points=total_points,
                                       earned_points=earned_points)
                ]
            })
        self['cells'] = cells
        # Fixes the new name.
        if 'metadata' in self and 'colab' in self['metadata']:
            cm = self['metadata']['colab']
            if 'name' in cm:
                cm['name'] = new_name

    def import_metadata(self, other):
        """Imports the nbgrader metadata from other to self.
        If this is not possible, raises CannotImportMetadata.
        """
        if len(self['cells']) != len(other['cells']):
            raise CannotImportMetadata("The notebook has the wrong number of cells")
        for i, selfc in enumerate(self['cells']):
            otherc = other['cells'][i]
            if selfc['cell_type'] != otherc['cell_type']:
                raise CannotImportMetadata("Corresponding cells have different type")
            # If present, imports nbgrader metadata.
            if 'nbgrader' in otherc['metadata']:
                selfc['metadata']['nbgrader'] = otherc['metadata']['nbgrader']
            if 'deletable' in otherc['metadata']:
                selfc['metadata']['deletable'] = otherc['metadata']['deletable']
            if 'editable' in otherc['metadata']:
                selfc['metadata']['editable'] = otherc['metadata']['editable']


    def get_result(self):
        return dict(self)

class SafeHash(object):
    """To undo the Python 3 harm."""
    def __init__(self):
        self.h = hashlib.sha1()

    def update(self, s):
        ss = s if isinstance(s, bytes) else s.encode('utf8')
        self.h.update(ss)

    def hexdigest(self):
        return self.h.hexdigest()


def watermark_file(filepath, email, key=""):
    """Watermarks a file for a particular student, returning the file
    as a string.  The file must be of type notebook.
    :param filepath: path of file to watermark
    :param email: email of student.
    :param key: key to be used to watermark."""
    # Computes the signature
    h = SafeHash()
    h.update(key)
    h.update(email)
    sig = h.hexdigest()
    # Reads the file.
    with open(filepath) as f:
        d = json.load(f)
        d["metadata"]["test_info"] = {"id": sig}
    # Returns the string.
    return json.dumps(d, indent=4)


def check_watermark(filepath, email, key=""):
    """Checks whether a given file has a correct watermark.
    Return: (valid, sig), consisting of a pair with the valid boolean, and the sig
    in the file.
    """
    try:
        with open(filepath) as f:
            d = json.load(f)
            if 'metadata' not in d:
                return True, ""
            if 'test_info' not in d['metadata']:
                return True, ""
    except:
        traceback.print_exc()
        return False, None
    sig = d['metadata']['test_info']['id']
    # Computes the signature
    h = SafeHash()
    h.update(key)
    h.update(email)
    sigp = h.hexdigest()
    return (sigp == sig), sig

class Namespace:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

# The following is for testing.
if __name__ == '__main__':
    import json
    args = Namespace()
    with open("somefile.ipynb") as f:
        nb = Notebook(json.load(f), args)
    nb.add_feedback("newname")
    print(json.dumps(nb, indent=2))

