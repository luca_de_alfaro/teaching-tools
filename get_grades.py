# Copyright Luca de Alfaro, 2019
# BSD License

import argparse
import csv
import datetime
import yaml

from nbgrader.api import Gradebook, MissingEntry

def main(args):

    with open(args.settings, 'r') as f:
        settings = yaml.load(f, Loader=yaml.SafeLoader)

    csv_fn = ('grades_' + args.assignment + '_' +
              datetime.datetime.now().isoformat().replace(':', '-').replace('.','-') + '.csv')
    with open(csv_fn, 'w', newline='') as csvfile:
        fieldnames=['email', 'max_grade', 'grade']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        # Create the connection to the database
        with Gradebook('sqlite:///'  + settings['class_path'] + '/gradebook.db') as gb:
            # Finds the assignment
            assignment = gb.find_assignment(args.assignment)
            # Gets the student grades in the assignment.
            for student in gb.students:
                grade = 0.
                try:
                    subm = gb.find_submission(assignment.name, student.id)
                    grade = subm.score
                except MissingEntry:
                    pass
                writer.writerow({'email': student.id, 'max_grade': assignment.max_score, 'grade': grade})

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--assignment', type=str, default=None,
                        help='Assignment for which to get the grades.')
    # Default is fine.
    parser.add_argument('--settings', type=str, default='settings.yaml',
                        help='Settings file to be used')
    args = parser.parse_args()
    main(args)