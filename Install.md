# Installation

conda install jupyter
conda install -c conda-forge nbgrader

pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib

Go to https://developers.google.com/drive/api/v3/quickstart/python
and get the credentials.json file. 

jupyter nbextension install --sys-prefix --py nbgrader --overwrite
jupyter nbextension enable --sys-prefix --py nbgrader
jupyter serverextension enable --sys-prefix --py nbgrader

