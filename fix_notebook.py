# Copyright Luca de Alfaro, 2020
# BSD License

import argparse
import datetime
import json
import os
import shutil
import traceback
import yaml

from notebook_utils import Notebook, CannotImportMetadata

AUTOGRADE_LINE = 'nbgrader autograde --student="{email}" --force {hw}\n'

def main(args):
    with open(args.settings, 'r') as f:
        settings = yaml.load(f, Loader=yaml.SafeLoader)
    graded_dir = os.path.join(settings['class_path'], 'autograded')
    submission_dir = os.path.join(settings['class_path'], 'submitted')
    release_dir = os.path.join(settings['class_path'], 'release', args.assignment)

    # Reads the assigned notebook.
    assignment_file = None
    with os.scandir(release_dir) as it:
        for entry in it:
            if entry.name.endswith('.ipynb') and entry.is_file():
                assignment_file = entry
                break
    assert assignment_file is not None, "Assignment not found"
    with open(entry) as f:
        assigned_notebook = Notebook(json.load(f), args)

    # Fixes the student's work.
    grading_script = ""
    for email in args.email.split(','):
        # Reads the student notebook.
        submitted_notebook = None
        student_submission_dir = os.path.join(submission_dir, email, args.assignment)
        student_graded_dir = os.path.join(graded_dir, email, args.assignment)
        with os.scandir(student_submission_dir) as it:
            for entry in it:
                if entry.is_file() and entry.name.endswith('.ipynb'):
                    with open(entry) as f:
                        submitted_notebook = Notebook(json.load(f), args)
                    break
        if submitted_notebook is not None:
            try:
                submitted_notebook.import_metadata(assigned_notebook)
                # Renames original file.
                shutil.move(entry.path, entry.path + ".orig")
                # Writes notebook in place.
                with open(entry.path, mode='w', newline='') as out_f:
                    json.dump(submitted_notebook.get_result(), out_f, indent=2)
                # Removes autograding result.
                with os.scandir(student_graded_dir) as gr:
                    for graded_entry in gr:
                        if graded_entry.is_file() and graded_entry.name.endswith('.ipynb'):
                            os.remove(graded_entry.path)
                # Puts the assignment in the ones to regrade
                grading_script += AUTOGRADE_LINE.format(email=email, hw=args.assignment)
                print("Fixed", email)
            except CannotImportMetadata:
                print("Fixing for student", email, "failed")
                traceback.print_exc()
    repair_fn = 'regrade_' + datetime.datetime.now().isoformat().replace(':', '-').replace('.','-') + '.sh'
    with open(repair_fn, "w") as repair_f:
        repair_f.write(grading_script)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # Needed.
    parser.add_argument('-a', '--assignment', type=str, default=None,
                        help='Assignment to assign.  Use either this or a file.')
    parser.add_argument('--email', type=str, default=None,
                        help='Comma-separated list of emails of notebooks to fix.')
    # Default is fine.
    parser.add_argument('--settings', type=str, default='settings.yaml',
                        help='Settings file to be used')
    args = parser.parse_args()
    main(args)
