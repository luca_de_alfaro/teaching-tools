# Copyright Luca de Alfaro, 2019
# BSD License

import argparse
import os
import pickle
import yaml

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

from file_distributor import FileDistributor
import notebook_utils

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets',
          'https://www.googleapis.com/auth/drive']
# https://www.googleapis.com/auth/spreadsheets for r/w


def read_class_list(settings, sheet_service, do_sort=True):
    """Reads the class list."""
    class_list = []
    # Reads from a spreadsheet.
    range_name = settings['class_list_sheet'] + '!' + 'A1:Z'
    spreadsheets = sheet_service.spreadsheets()
    mysheet = spreadsheets.values().get(
        spreadsheetId=settings['class_list_id'],
        range=range_name).execute()
    values = mysheet.get('values', [])
    assert values, "Empty class list!"
    for row in values:
        email_idx = ord(settings['class_list_column']) - ord('A')
        email = row[email_idx]
        if email and '@' in email:
            class_list.append(email)
    if do_sort:
        class_list.sort()
    return class_list


def main(args):
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            try:
                creds.refresh(Request())
            except:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server()
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    drive_service = build('drive', 'v3', credentials=creds)
    sheet_service = build('sheets', 'v4', credentials=creds)

    with open(args.settings, 'r') as f:
        settings = yaml.load(f, Loader=yaml.SafeLoader)

    # Reads the class list
    if args.email is None:
        class_list = read_class_list(settings, sheet_service, do_sort=False)
    else:
        class_list = args.email.split(',')
        
    # Takes care of the resume argument.
    if args.resume is not None and args.resume in class_list:
        class_list = class_list[class_list.index(args.resume) + 1:]
    print("Class list:", class_list)

    # Reads files to be assigned.
    files_to_distribute = []
    release_dir = os.path.join(settings['class_path'], 'release', args.assignment)
    with os.scandir(release_dir) as it:
        for entry in it:
            if entry.name.endswith('.ipynb') and entry.is_file():
                files_to_distribute.append(entry)

    # Creates the file distributor.
    file_distributor = FileDistributor(drive_service,
                                       settings['class_name'],
                                       settings['share_folder_id'])

    # Distributes the files.
    for email in class_list:
        for entry in files_to_distribute:
            if settings.get('watermark_key') is None:
                # Just distibutes the file.
                file_distributor.distribute_file(
                    email, entry.path, entry.name,
                    'application/vnd.google.colaboratory',
                    mode='writer'
                )
            else:
                # We need to watermark the file.
                watermarked_content = notebook_utils.watermark_file(
                    entry.path, email, key=settings['watermark_key'])
                bytes = watermarked_content.encode('utf8')
                file_distributor.distribute_bytes(
                    email, bytes, entry.name,
                    'application/vnd.google.colaboratory',
                    mode='writer'
                )
    print("All done")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # Needed.
    parser.add_argument('-a', '--assignment', type=str, default=None,
                        help='Assignment to assign.  Use either this or a file.')
    parser.add_argument('--email', type=str, default=None,
                        help='If specified, distributes the homework only to this comma-separated list of emails.')

    # Default is fine.
    parser.add_argument('--settings', type=str, default='settings.yaml',
                        help='Settings file to be used')

    parser.add_argument('-r', '--resume', type=str, default=None,
                        help='Resume after this student')
    args = parser.parse_args()
    main(args)
