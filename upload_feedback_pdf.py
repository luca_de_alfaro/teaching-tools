# Copyright Luca de Alfaro, 2019
# BSD License

import argparse
import os
import pickle
import pdfkit
import yaml

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

from file_distributor import FileDistributor

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets',
          'https://www.googleapis.com/auth/drive']
# https://www.googleapis.com/auth/spreadsheets for r/w


def main(args):
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            try:
                creds.refresh(Request())
            except:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server()
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    drive_service = build('drive', 'v3', credentials=creds)

    with open(args.settings, 'r') as f:
        settings = yaml.load(f, Loader=yaml.SafeLoader)

    # Creates the file distributor.
    file_distributor = FileDistributor(drive_service,
                                       settings['class_name'] + " " + "Feedback",
                                       settings['feedback_folder_id'])

    # Now uploads each piece of feedback.
    feedback_dir = os.path.join(settings['class_path'], 'feedback')
    emails = [e for e in os.listdir(feedback_dir) if not e.startswith('.')]
    emails.sort()
    for email in emails:
        student_dir = os.path.join(feedback_dir, email, args.assignment)
        if os.path.exists(student_dir):
            with os.scandir(student_dir) as it:
                for entry in it:
                    if entry.is_file() and entry.name.endswith('.html'):
                        # We convert the file to pdf.
                        pdf_name = '.'.join(entry.name.split('.')[:-1] + ['pdf'])
                        pdf_path = os.path.join(student_dir, pdf_name)
                        pdfkit.from_file(entry.path, pdf_path, options={'--quiet': ''})
                        file_distributor.distribute_file(
                            email, pdf_path, pdf_name, 'application/pdf',
                            update=args.update)
    print("All done")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # Needed.
    parser.add_argument('-a', '--assignment', type=str, default=None,
                        help='Assignment for which to give feedback')
    parser.add_argument('-u', '--update', action='store_true', default=False,
                        help='Updates existing feedback if it exists.')
    # The default is fine.
    parser.add_argument('--settings', type=str, default='settings.yaml',
                        help='Settings file to be used')
    args = parser.parse_args()
    main(args)
